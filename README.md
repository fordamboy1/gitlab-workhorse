# GitLab Workhorse has moved

As part of [moving GitLab Workhorse development into gitlab-org/gitlab](https://gitlab.com/groups/gitlab-org/-/epics/4826), 
the canonical source for GitLab Workhorse is now [gitlab-org/gitlab/workhorse](https://gitlab.com/gitlab-org/gitlab/tree/master/workhorse). 

To see the old contents of this repo, change the branch to e.g. [`8-65-stable`](https://gitlab.com/gitlab-org/gitlab-workhorse/-/tree/8-65-stable).

New Workhorse issues should go on the gitlab-org/gitlab [issue
tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issue%5bdescription%5d=%2flabel%20%7eworkhorse).
New Merge Requests should also go to gitlab-org/gitlab.

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.
